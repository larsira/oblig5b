package no.ntnu.idatx2001.oblig5.zoo;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;
import no.ntnu.idatx2001.oblig5.zoo.animals.ScandinavianWildAnimal;

public class AnimalClient {
  private Set<ScandinavianWildAnimal> animals;
  private static final Logger log = Logger.getLogger(AnimalClient.class.getName());
  
  /**
   * Main method for the application.
   * @param args String arguments for execution.
   */
  public static void main(String[] args) {
    AnimalClient application = new AnimalClient();
    application.init();
    application.createAnimals();
    application.printAnimals();
  }
  
  /**
   * Initializes the client.
   */
  private void init() {
    this.animals = new HashSet<>();
    log.fine("Application initialized");
  }
  
  /**
   * Creates animals based on pre-defined values, and adds them to the set containing
   * <code>ScandinavianWildAnimal<code/> objects.
   */
  private void createAnimals() {
    WildAnimalFactory      animalFactory = WildAnimalFactory.getInstance();
    LocalDate              date =  LocalDate.of(2020,1,2);
    LocalDate              birthDate = LocalDate.of(2018,2,23);
    
    ScandinavianWildAnimal boline = animalFactory.newFemaleBear(date, "Boline", birthDate, "Inngjerding 4");
    this.animals.add(boline);
    
    ScandinavianWildAnimal ulla = animalFactory.newFemaleWolf(date, "Ulla", birthDate, "Inngjerding 2");
    this.animals.add(ulla);
    
    ScandinavianWildAnimal bjornar = animalFactory.newMaleBear(date, "Bjørnar", birthDate, "Inngjerding 4");
    this.animals.add(bjornar);
    
    ScandinavianWildAnimal ulrik = animalFactory.newMaleWolf(date, "Ulrik", birthDate, "Inngjerding 2");
    this.animals.add(ulrik);
  }
  
  /**
   * Prints the information of all <code>ScandinavianWildAnimal</code> objects.
   */
  private void printAnimals() {
    for(ScandinavianWildAnimal animal: animals) {
      System.out.println(animal.printInfo());
      System.out.println("");
    }
  }
}
