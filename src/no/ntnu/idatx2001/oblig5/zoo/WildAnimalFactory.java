package no.ntnu.idatx2001.oblig5.zoo;

import java.time.LocalDate;
import no.ntnu.idatx2001.oblig5.zoo.animals.FemaleIndividual;
import no.ntnu.idatx2001.oblig5.zoo.animals.MaleIndividual;
import no.ntnu.idatx2001.oblig5.zoo.animals.ScandinavianWildAnimal;

public class WildAnimalFactory {
  private static final String NOR_BEAR = "Bjørn";
  private static final String LAT_BEAR = "Ursidae";
  private static final String NOR_WOLF = "Ulv";
  private static final String LAT_WOLF = "Canis lupus";
  private static final String LAT_WOLF_FAMILY = "Canis";
  
  private static WildAnimalFactory instance;
  
  /**
   * Default constructor.
   */
  private WildAnimalFactory() {
    //Intentionally left empty
  }
  
  /**
   * Creates an instance of WildAnimalFactory if one does not already exist, will always return a
   * WildAnimalFactory.
   * @return a WildAnimalFactory
   */
  public static WildAnimalFactory getInstance() {
    if(instance == null) {
      instance = new WildAnimalFactory();
    }
    return instance;
  }
  
  /**
   * Creates and returns a new male bear with the specified attributes.
   * @param dateOfArrival Date of Arrival for the bear
   * @param name The chosen "pet name" for the bear
   * @param dateOfBirth when the bear was born
   * @param address where the bear is kept
   * @return ScandinavianWildAnimal representing a male bear
   */
  public ScandinavianWildAnimal newMaleBear(LocalDate dateOfArrival,
                                            String name, LocalDate dateOfBirth,
                                            String address) {
    return new MaleIndividual(NOR_BEAR, LAT_BEAR, LAT_BEAR, dateOfArrival, name, dateOfBirth, true,
                              address);
  }
  
  /**
   * Creates and returns a new female bear with the specified attributes.
   * @param dateOfArrival Date of Arrival for the bear
   * @param name The chosen "pet name" for the bear
   * @param dateOfBirth when the bear was born
   * @param address where the bear is kept
   * @return ScandinavianWildAnimal representing a female bear
   */
  public ScandinavianWildAnimal newFemaleBear(LocalDate dateOfArrival,
                                              String name, LocalDate dateOfBirth,
                                              String address) {
    return new FemaleIndividual(NOR_BEAR, LAT_BEAR, LAT_BEAR, dateOfArrival, name, dateOfBirth, true,
                                address, 0);
  }
  
  /**
   * Creates and returns a new male wolf with the specified attributes.
   * @param dateOfArrival Date of Arrival for the bear
   * @param name The chosen "pet name" for the bear
   * @param dateOfBirth when the bear was born
   * @param address where the bear is kept
   * @return ScandinavianWildAnimal representing a male wolf
   */
  public ScandinavianWildAnimal newMaleWolf(LocalDate dateOfArrival,
                                            String name, LocalDate dateOfBirth,
                                            String address) {
    return new MaleIndividual(NOR_WOLF, LAT_WOLF, LAT_WOLF_FAMILY, dateOfArrival, name,
                                dateOfBirth, true, address);
  }
  
  /**
   * Creates and returns a new female wolf with the specified attributes.
   * @param dateOfArrival Date of Arrival for the bear
   * @param name The chosen "pet name" for the bear
   * @param dateOfBirth when the bear was born
   * @param address where the bear is kept
   * @return ScandinavianWildAnimal representing a female wolf
   */
  public ScandinavianWildAnimal newFemaleWolf(LocalDate dateOfArrival,
                                              String name, LocalDate dateOfBirth,
                                              String address) {
    return new FemaleIndividual(NOR_WOLF, LAT_WOLF, LAT_WOLF, dateOfArrival,name,dateOfBirth,true,
                                address, 0);
  }
}
